#!/bin/bash

set -x;

apt-get update -yq

packages=(

  # SDKs and CASE
  build-essential
  cargo
  default-jdk

  docker.io
  docker-compose
  docker-registry

  gawk
  git
  golang

  python3
  python3-dev
  python3-dateutil
  python3-jedi
  python3-matplotlib
  python3-pip
  python3-setuptools
  python3-wheel
  python3-venv

  shellcheck
  vim-python-jedi
  xserver-xephyr
  yad

  # Editors
  emacs-gtk
  neovim-qt
  vim-gtk3

  # Browsers and web tools
  chromium
  curl
  firefox-esr
  wget

  # Networking
  dnsutils
  hping3
  netcat-openbsd
  netdata
  nmap
  mosh
  socat
  stunnel4
  tcpdump
  wireshark

  # Messaging
  telegram-desktop

  # Bash tooling
  fortune
  htop
  inotify-tools
  rsync

  # LaTeX
  texlive
  texlive-xetex
  texlive-science

  # One ring to bind them all
  tmux
)

arguments=(
   -y
   -q
  --no-install-recommends
)

apt-get install "${arguments[@]}" "${packages[@]}"
