#!/bin/bash

env; set -x

apt-get full-upgrade -y
apt-get autoremove -y
apt-get clean
