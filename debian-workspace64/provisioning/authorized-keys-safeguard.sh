#!/bin/bash

set -x

KEY_ORI_FILE="$HOME/.ssh/authorized_keys"
KEY_BKP_FILE="$HOME/.ssh/vagrant.provisioning.ori.authorized_keys"

cp "$KEY_ORI_FILE" "$KEY_BKP_FILE"
