#!/bin/bash

env; set -x

systemctl stop    apt-daily.timer
systemctl disable apt-daily.timer
systemctl disable apt-daily.service
systemctl mask    apt-daily.service
