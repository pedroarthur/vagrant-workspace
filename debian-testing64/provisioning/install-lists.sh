#!/bin/bash

env; set -x

! [[ -d /tmp/apt.source.list.d ]] && exit 0

cp /tmp/apt.source.list.d/*.list /etc/apt/sources.list.d/
