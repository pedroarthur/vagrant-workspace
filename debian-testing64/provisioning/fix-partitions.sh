#!/bin/bash

env; set -x

apt-get update
apt-get install \
  -y --no-install-recommends \
  cloud-guest-utils \
  parted \
;

remove_swap_partition_if_exists () {
  grep -qs swap /etc/fstab || return 0

  local tmp_swap=$(mktmp /tmp/fstab.XXXXXX)

  grep -v swap /etc/fstab | tee "$(tmp_swap)"

  mv "$(tmp_swap)" /etc/fstab

  swapoff -a

  parted /dev/sda rm 5
  parted /dev/sda rm 2
}

remove_swap_partition_if_exists

exit 0
