#!/bin/bash

env; set -x

cp /tmp/systemd/*.service /etc/systemd/system/

systemctl daemon-reload
systemctl enable resize-partition.service
