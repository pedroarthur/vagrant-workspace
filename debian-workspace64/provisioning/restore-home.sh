#!/bin/bash

set -x;

home_seed_file="/vagrant/home.tar.gz"

[[ -r "$home_seed_file" ]] \
  || exit 0


exit_code=0

tar -C "$HOME" -vxzf "$home_seed_file" || {
  echo "error: failed to restore home"
  ((exit_code|=1))
}

exit $exit_code
