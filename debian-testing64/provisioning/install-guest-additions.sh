#!/bin/bash

env; set -x

oracle_key_file=$(mktemp /tmp/oracle_key_file.XXXXX.sc)

wget -v -O "$oracle_key_file" \
  https://www.virtualbox.org/download/oracle_vbox_2016.asc

cat "$oracle_key_file"

apt-key add "$oracle_key_file"

apt-get update
apt-get install \
  -y --no-install-recommends \
  linux-image-amd64 \
    linux-headers-amd64 \
  virtualbox-guest-utils \
    `# virtualbox-guest-dkms` \
;
