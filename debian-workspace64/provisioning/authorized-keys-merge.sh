#!/bin/bash

set -x

KEY_MRG_FILE="$HOME/.ssh/authorized_keys2"

KEY_BKP_FILE="$HOME/.ssh/vagrant.provisioning.ori.authorized_keys"
KEY_MRG_TEMP="$HOME/.ssh/vagrant.provisioning.mrg.authorized_keys"

cat "$KEY_BKP_FILE" "$KEY_MRG_FILE" > "$KEY_MRG_TEMP"
mv  "$KEY_MRG_TEMP" "$KEY_MRG_FILE"
