SHELL := bash

export VAGRANT_VAGRANTFILE = $(*F).vagrant

%.id: VAGRANT_BOX_UP_ID  = $(PWD)/$(*D)/.vagrant/machines/default/virtualbox/id
%.id: VAGRANT_BOX_UP_LOG = $(PWD)/$(*).up.log

%.id: %.vagrant
	cd "$(@D)"; vagrant up --provision | tee "$(VAGRANT_BOX_UP_LOG)"
	cd "$(@D)"; vagrant halt
	cp "$(VAGRANT_BOX_UP_ID)" "$(@)"

%.box: VAGRANT_PACKAGE_TEMP_FILE = $(@F).part
%.box: VAGRANT_PACKAGE_TEMP_FQFN = $(@D)/$(VAGRANT_PACKAGE_TEMP_FILE)

%.box: %.id
	-rm  "$(VAGRANT_PACKAGE_TEMP_FQFN)"
	 cd "$(@D)"; vagrant package --output "$(VAGRANT_PACKAGE_TEMP_FILE)"
	 mv "$(VAGRANT_PACKAGE_TEMP_FQFN)" "$(@)"

%.meta: VAGRANT_PACKAGE_METADATA = $(*F).json
%.meta: VAGRANT_BOX_UP_METADATA  = $(*D)/.vagrant/machines/default/virtualbox/box_meta

%.meta: %.box %.json
	cd "$(@D)"; vagrant box add --force  "$(VAGRANT_PACKAGE_METADATA)"
	cp "$(VAGRANT_BOX_UP_METADATA)" "$(@)"
	cd "$(@D)"; vagrant destroy -f

%.up: %.id
	cd "$(@D)"; vagrant up

# simple names
DEBIAN_TESTING_64   = debian-testing64/debian-testing64.meta
DEBIAN_PLASMA_64    = debian-plasma64/debian-plasma64.meta
DEBIAN_WORKSPACE_64 = debian-workspace64/debian-workspace64.up

# dependencies
$(DEBIAN_PLASMA_64): $(DEBIAN_TESTING_64)
$(DEBIAN_WORKSPACE_64): $(DEBIAN_PLASMA_64)

# simpler names
debian-testing64 : $(DEBIAN_TESTING_64)
debian-plasma64  : $(DEBIAN_PLASMA_64)

debian-workspace64: $(DEBIAN_WORKSPACE_64)
